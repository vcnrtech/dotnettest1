﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EncryptionDecryptionPOC.Controllers
{
    public class EncryptionDecryptionController : Controller
    {
        // GET: EncryptionDecryption
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Encrypt()
        {
            ViewBag.estring = null;
            return View();
        }

        [HttpPost]
        public ActionResult Encrypt(string encryptionstring)
        {
            SecurePasswordHasher objSecurePasswordHasher = new SecurePasswordHasher();
            string estring = objSecurePasswordHasher.Encrypt(encryptionstring);
            ViewBag.estring = estring;
            return View();
        }

        public ActionResult Decrypt()
        {
            ViewBag.dstring = null;
            return View();
        }

        [HttpPost]
        public ActionResult Decrypt(string decryptionstring)
        {
            SecurePasswordHasher objSecurePasswordHasher = new SecurePasswordHasher();
            string dstring = objSecurePasswordHasher.Decrypt(decryptionstring);
            ViewBag.dstring = dstring;
            return View();
        }
    }
}